
// FUNCION DE TABS

$('[data-tab]').click(function() {
	var showTab = $(this).attr('data-tab');
	$('#' + showTab).siblings('.tab-content').fadeOut(300);
	$('#' + showTab).fadeIn(300);
	$(this).parent().siblings().removeClass('current');
	$(this).parent().addClass('current');

});

$('body').on('click', '[data-tab]', function() {
    $('[data-tab]').click(function() {
    	var showTab = $(this).attr('data-tab');
    	$('#' + showTab).siblings('.tab-content').fadeOut(300);
    	$('#' + showTab).fadeIn(300);
    	$(this).parent().siblings().removeClass('current');
    	$(this).parent().addClass('current');
    });
});

// OCULTAR FILTRO CIUDADES

$('#userinfo-tab').click(function() {
	$('.country-select').addClass('d-none');
});
$('#misrecos-tab').click(function() {
	$('.country-select').removeClass('d-none');
});
$('#guardadas-tab').click(function() {
	$('.country-select').removeClass('d-none');
});

// Filtro ciudades

$('.dropdown.country-select').on('shown.bs.dropdown', function () {
  $('.country-select-btn').addClass('open show');
})

$('.dropdown.country-select').on('hidden.bs.dropdown', function () {
  $('.country-select-btn').removeClass('open show');
})


// MENU RESPONSIVE

$('.open-nav').click(function() {
	$(this).toggleClass('close-nav');
	$('.toggle-menu').slideToggle(300);
	$('body').toggleClass('no-scroll');
});

$('.open-search button').click(function() {
	$(this).parent().toggleClass('close-search',300);
	$('.header-nav ul:not(.user-top ul)').animate({'opacity': 'toggle'},100);
	$('.nav-mobile-header a .burger, .nav-mobile-header .logo-header').animate({'opacity': 'toggle'},100);
	// $('.typeahead').focus();
  if ($(this).hasClass('close-search')) {
    $('.typeahead').typeahead('close');
  }
});

$('.nav-mobile-header .open-search button').click(function() {	
	$('body').toggleClass('no-scroll-overlay');
})


// AGREGO CLASE AL HEADER CUANDO SCROLLEO

$(window).scroll(function() {
	var windscroll = $(window).scrollTop();
	if (windscroll >= 120) {
		$('header').addClass('header-scroll');	
		$('.nav-mobile-header').addClass('header-scroll');	
	} else {
		$('header').removeClass('header-scroll');	
		$('.nav-mobile-header').removeClass('header-scroll');		
	}
}).scroll();


// CAMBIAR IMAGEN PORTADA

$('.change-pic').click(function() {
	$('#cover-pic').click();
});

$('#cover-pic').change(function() {
	src = window.URL.createObjectURL(this.files[0]);
	$('.myprofile-cover').css('background-image', 'url(' + src + ')');
	$('.myprofile-cover').addClass('myprofile-cover-img');
  $('.change-pic').slideUp(300);
  $('.change-pic-twice').slideDown(300);
  $('.delete-pic').slideDown();
  if ($('.myprofile-cover').hasClass('myprofile-cover-img')) {
    $('.change-pic').fadeOut();
    $('.change-pic-twice').fadeIn();
  }
});

$('.delete-pic').click(function() {
    $('.myprofile-cover').removeClass('myprofile-cover-img').css({"background-color": "#43D6C8", "background-image": "unset"});
    $('.change-pic').slideDown(300);
    $('.change-pic-twice').slideUp(300);
    $(this).slideUp();
});

$('.change-pic-twice').click(function() {
  $('#cover-pic').click();
});


// CAMBIAR IMAGEN PERFIL

$('.change-pic-profile').click(function() {
	$('#profile-pic').click();
});

$('#profile-pic').change(function() {
	src = window.URL.createObjectURL(this.files[0]);
  $('.user-img').css('background-image', 'url(' + src + ')');
  $('.user-img').css('box-shadow', 'inset 0px 0px 10px rgba(0,0,0,0.04)');
  $('.user-img').css('-webkit-box-shadow', 'inset 0px 0px 10px rgba(0,0,0,0.04)');
  $('.user-img').css('-moz-box-shadow', 'inset 0px 0px 10px rgba(0,0,0,0.04)');
});


// BUSCADOR TYPEAHEAD

var data_users = [
     {name: "Bastian Gomez", picture: "img/users/user-1.jpg", commonfriends: "5"}
    ,{name: "Cheche Enriquez", picture: "img/users/user-2.jpg", commonfriends: "5"}
    ,{name: "Bartolome Mitre", picture: "img/users/user-3.jpg", commonfriends: "8"}
    ,{name: "Bartolo Esteban Coco", picture: "img/users/martinruiz.png", commonfriends: "5"}
    ,{name: "Bernardo Esteban Coco", picture: "img/users/user-1.jpg", commonfriends: "5"}
    ,{name: "Bernardo Esteban Coco", picture: "img/users/user-1.jpg", commonfriends: "5"}
    ,{name: "Bella Straus", picture: "img/users/user-2.jpg", commonfriends: "9"}
    ,{name: "Martin Ruiz Coco", picture: "img/users/user-3.jpg", commonfriends: "10"}
];

var data_cities = [
     {cityname: "Barcelona", countryname: "España", continentname: "Europa"}
    ,{cityname: "Bruselas", countryname: "Bélgica", continentname: "Europa"}
    ,{cityname: "Buenos Aires", countryname: "Argentina", continentname: "América del Sur"}
    ,{cityname: "Budapest", countryname: "Hungría", continentname: "Europa"}
    ,{cityname: "Berlín", countryname: "Alemania", continentname: "Europa"}
    ,{cityname: "Roma", countryname: "Italia", continentname: "Europa"}
];

var users = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  local: data_users
});

var cities = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('cityname'),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  local: data_cities
});

var userssearch = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  local: data_users
});

users.initialize();
cities.initialize();
userssearch.initialize();

$('.typeahead').typeahead(	
    {
        hint: true,
        highlight: true,
        minLength: 3,	        
    },    
    {
        name: 'cities',
        displayKey: 'cityname',
        source: cities.ttAdapter(),
        templates: {
            // header: '<h3 class="league-name">Ciudades</h3>',
            suggestion: Handlebars.compile('<div class="city-result"><img src="img/city-icon.svg" /><div class="search-title"><h4>{{cityname}}</h4><small>{{countryname}}, {{continentname}}</small></div>'),
            footer : [ '<div class="more-results">', '<a href="javascript:void(0);" data-toggle="modal" data-target="#ResultadosBusqueda">Ver todos los resultados</a>','<div>'].join('\n')
             
        },
        limit: 4,
    },
    {
        name: 'users',
        displayKey: 'name',
        source: users.ttAdapter(),
        templates: {
        	// header: '<h3 class="league-name">Usuarios</h3>',            
            suggestion: Handlebars.compile('<div class="user-result"><img src="{{picture}}"/><div class="search-title"><h4>{{name}}</h4><small>{{commonfriends}} conexiones</small></div>'),            
      footer : '<div class="more-results"><a href="javascript:void(0);" data-toggle="modal" data-target="#ResultadosBusqueda">Ver todos los resultados</a><div>',
      empty : '<h3 class="no-result-search">No existen recos de esa ciudad o ese usuario no existe</h3>'  
        },
        limit: 4,
    },

).on('blur', function () {
        ev = $.Event("keydown");
        ev.keyCode = ev.which = 40;
        $('.typeahead').trigger(ev);
        return true;
    });


users.initialize();
$('.search-users').typeahead(  
    {
        hint: true,
        highlight: true,
        minLength: 1,           
    },    
    {
        name: 'users',
        displayKey: 'name',
        source: userssearch.ttAdapter(),
        templates: {                     
            suggestion: Handlebars.compile('<div class="user-result" onclick="showChecked()" id="user-checked"><img src="{{picture}}"/><div class="search-title"><h4>{{name}}</h4><div class="round"><input type="checkbox" name="checked"/><label></label></div></div></div>')                
        },
    }
).on('blur', function () {
        ev = $.Event("keydown");
        ev.keyCode = ev.which = 40;
        $('.typeahead').trigger(ev);
        return true;
    });


cities.initialize();
$('.search-cities').typeahead(  
      {
          hint: true,
          highlight: true,
          minLength: 3,           
      },    
      {
        name: 'cities',
        displayKey: 'cityname',
        source: cities.ttAdapter(),
        templates: {
            suggestion: Handlebars.compile('<div class="city-result"><img src="img/city-icon.svg" /><div class="search-title"><h4>{{cityname}}</h4></div></div>')           
          },
      }
  ).on('blur', function () {
          ev = $.Event("keydown");
          ev.keyCode = ev.which = 40;
          $('.typeahead').trigger(ev);
          return true;
      });



// Modal Share - Copy URL page
function Copy() {
  var Url = document.getElementById("url");
  Url.innerHTML = window.location.href;
  console.log(Url.innerHTML)
  Url.select();
  document.execCommand("copy");
}



// $(document).ready(function() {  
//   if ($('.tt-menu div:last-child').css('display') == 'none')
// {
//   $('.tt-menu .more-results ').css('display', 'block')
// }
//   });  


//*------- NEW JS *-------//

// CHARACTERS COUNTER
// Add id to character inputs and inputs
var idNumberCounter = 0;

$('.js-char-counter-container').each(function() {
  idNumberCounter++;
  $(this).children('.input').attr('id', 'js-char-counter-input-' + idNumberCounter);
  $(this).children('.js-char-counter').addClass('js-char-counter-input-' + idNumberCounter);
});

function charCount (inputVal) {
  if(inputVal) {
    var length = inputVal.value.length,
        maxValue = inputVal.getAttribute("maxlength");

    if (length <= maxValue) {
      $('.' + inputVal.id).text(maxValue - length);
    }
  }
}

charCount();

// DESCRIPTION HEIGHT INCREASE
function descriptionHeightIncrease( descriptionInput ) {
  descriptionInput.style.height = "";
  descriptionInput.style.height = descriptionInput.scrollHeight + "px";
}

var imagesSrcArray = [];

// UPLOAD IMAGE

// Modal: City card
$(".modal-city-card .js-input-file__button").click(function () {
  $('.modal-city-card #js-input-file__input').click();
});

$('.modal-city-card #js-input-file__input').change(function () {
  src = window.URL.createObjectURL(this.files[0]);

  // Multiple files
  imagesSrcArray.push(src);
  $('.modal-city-card .input-file__upload-container').append($('<div/>', { 'class': 'input-file input-file__upload' }));
  $('.modal-city-card .input-file__upload').append($('<div/>', { 'class': 'input-file__upload-img' }));
  $('.modal-city-card .input-file__upload').append($('<div/>', { 'class': 'input-file__hover-cross' }));


  $.each(imagesSrcArray, function(el){
    $('.modal-city-card .input-file__upload-img').last().css('background-image', 'url(' + imagesSrcArray[el] + ')');
    $('.modal-city-card .input-file__upload').last().addClass("input-file__upload--state-uploaded");
  } );

});

// Modal: Edit reco
$(".modal-edit-reco .js-input-file__button").click(function () {
  $('.modal-edit-reco #js-input-file__input').click();
});

$('.modal-edit-reco #js-input-file__input').change(function () {
  src = window.URL.createObjectURL(this.files[0]);

  // Multiple files
  imagesSrcArray.push(src);
  $('.modal-edit-reco .input-file__upload-container').append($('<div/>', { 'class': 'input-file input-file__upload' }));
  $('.modal-edit-reco .input-file__upload').append($('<div/>', { 'class': 'input-file__upload-img' }));
  $('.modal-edit-reco .input-file__upload').append($('<div/>', { 'class': 'input-file__hover-cross' }));


  $.each(imagesSrcArray, function (el) {
    $('.modal-edit-reco .input-file__upload-img').last().css('background-image', 'url(' + imagesSrcArray[el] + ')');
    $('.modal-edit-reco .input-file__upload').last().addClass("input-file__upload--state-uploaded");
  });

});

// Modal: New city
$(".modal-new-city .js-input-file__button").click(function () {
  $('.modal-new-city #js-input-file__input').click();
});

$('.modal-new-city #js-input-file__input').change(function () {
  src = window.URL.createObjectURL(this.files[0]);

  // One pic
  $('.modal-new-city #js-input-file__onepic').addClass('input-file__onepic--state-active');
  $('.modal-new-city #js-input-file__onepic').css('background-image', 'url(' + src + ')');

});

// Modal: Reco modal full
$(".modal-reco-full .js-input-file__button").click(function () {
  $('.modal-reco-full #js-input-file__input').click();
});

$('.modal-reco-full #js-input-file__input').change(function () {
  src = window.URL.createObjectURL(this.files[0]);

  // Multiple files
  imagesSrcArray.push(src);
  $('.modal-reco-full .input-file__upload-container').append($('<div/>', { 'class': 'input-file input-file__upload' }));
  $('.modal-reco-full .input-file__upload').append($('<div/>', { 'class': 'input-file__upload-img' }));
  $('.modal-reco-full .input-file__upload').append($('<div/>', { 'class': 'input-file__hover-cross' }));


  $.each(imagesSrcArray, function (el) {
    $('.modal-reco-full .input-file__upload-img').last().css('background-image', 'url(' + imagesSrcArray[el] + ')');
    $('.modal-reco-full .input-file__upload').last().addClass("input-file__upload--state-uploaded");
  });

});


// Dropdown replace button title to menu
$('.js-dropdown-titletobutton ul li').click(function() {
  $(this).parent().siblings('.dropdown-titletobutton-button').addClass("dropdown-titletobutton-button--state-active");
  $(this).parent().siblings('.dropdown-titletobutton-button').text($(this).text());
});

// HEART TOGGLE
$('#js-save__heart').click(function() {
  $('#js-save__heart-outline, #js-save__heart-full ').toggle();
});
$('#js-save__heart').click(function(){
  $('#js-save__heart-full').addClass('animate-heart');
});


//Modal: Remove Image
$('body').on( 'click', '.input-file__hover-cross', function() {
  $(this).closest('.input-file__upload').remove();
});


$('.modal-terms-and-conditions__items').click(function () {  
  
  $('.modal-terms-and-conditions__wrapper-right').animate({ scrollTop: 0 }, 0);

  $('.modal-terms-and-conditions__items').removeClass('active');
  $(this).addClass('active');

  var posTarget = $(this).attr('go-to');
  var pos = $('#'+posTarget).position();

  $('.modal-terms-and-conditions__wrapper-right').animate({ scrollTop: pos.top - 50 }, 0);


});

// Cambio de color icono guardado
// $('.modal-city-card-posted__btn-save').focus(function () {  
//   $('#guardado').attr("src", "img/suitcase-small-hover2.svg");
// });
// $('.modal-city-card-posted__btn-save').focusout(function () {  
//   $('#guardado').attr("src", "img/suitcase-small1.svg");
// });

//Scrolleo invisible Ocultar barra


// Nueva ciudad - Mostrar buscador de ciudades

$('.search-button').click(function () {  
  $('.modal-new-city__search-city').removeClass("d-none");
  $('.modal-new-city__main-content').addClass("d-none");
});




// TOM JS

// Slide modals reco

$('#seleccionar-ciudad').click(function () {  
  $('#RecoModalFull modal-dialog').addClass('transition-slide-out');
  $('#RecoModalFull').addClass('d-none');
  $('#RecoModalFull').removeClass('show');
 
});

// Close Modal

$('.close-icon').click(function () {  
  $('.modal').removeClass('d-none');
  $('.modal').removeClass('show');

});


// COOKIES SHOW - HIDE

  $(window).on("load", function() {
    $(".cookies").addClass("show-cookies");
});
   
$(".cookies-close").click(function() {
  $(".cookies").removeClass("show-cookies");
  $(".cookies").addClass("hide-cookies");
});


// function checked() {
//   $(".round label").css({"background-color": "#43d6c8"});
//   $(".round label").css({"border-color": "#43d6c8}"});
//   $(".round label").after().css({"opacity": "1"});
  
// }


function showChecked() {
  
  $('input[name=checked]').is(':checked');
  $('input[name=checked]').attr('checked');
  $('input[name=checked]').attr('checked', true);
  $('input[name=checked]').attr('checked', false);
};




// No scroll behind loader-page
// Agregale la clase no-scroll al body en el html

$(document).ready(function() { // When page finished loading
  setTimeout(function(){
    $('.loader-page').addClass('d-none');
    $('body').removeClass('no-scroll-loaderpage');
  }, 2000);
});


// Animacion boton guardadod

$('.modal-city-card-posted__btn-save').click(function () { 
  if($('.modal-city-card-posted__btn-save').hasClass('saved')){
    $('.modal-city-card-posted__btn-save').removeClass('saved');
    $('.modal-city-card-posted__btn-save-img').attr('src', './img/suitcase-small1.svg');
  }
  else{
    $('.modal-city-card-posted__btn-save').addClass('saved');
    $('.modal-city-card-posted__btn-save-img').attr('src', './img/suitcase-small-hover2.svg');
  }
});


// DROPDOWN NOTIFICACIONES Y LOGO PERFIL MENU

/*

 $('.header-nav-container #notiDropdown').click(function () {  
   if($('.userinfo-dropdown').hasClass('show')){
       $('.userinfo-dropdown').removeClass('show');
       $('.noti-dropdown').addClass('show');
   }
   else if($('.noti-dropdown').hasClass('show')) {
     $('.noti-dropdown').removeClass('show');
   }
   else{
     $('.noti-dropdown').addClass('show');
   }
 });


 $('.header-nav-container #userinfoDropdown').click(function () {  
   if($('.noti-dropdown').hasClass('show')){
     $('.noti-dropdown').removeClass('show');
     $('.userinfo-dropdown').addClass('show');
   }
   else if($('.userinfo-dropdown').hasClass('show')) {
     $('.userinfo-dropdown').removeClass('show');
   }
   else{
     $('.userinfo-dropdown').addClass('show');
   }
});


$(document).click(function() { 
  
  if(!("#notiDropdown").closest('#menucontainer').length && 
  $('#menucontainer').is(":visible")) {
    $('#menucontainer').hide();
  }        
});


$(document).click(function(event) { 
  $("#notiDropdown") = $(event.target);
  if(!("#notiDropdown").closest('#menucontainer').length && 
  $('#menucontainer').is(":visible")) {
    $('#menucontainer').hide();
  }        
});

*/
